<!DOCTYPE html>

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="../css/projects.css" rel="stylesheet" type="text/css">
        <link href="../css/commandes.css" rel="stylesheet" type="text/css">
        <link href="../css/rwd_commandes.css" rel="stylesheet" type="text/css">

        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>

        <script src="../js/vendor/jquery-parallax-1.1.3.js" type="text/javascript"></script>
        <script src="../js/interface.js" type="text/javascript"></script>
        <script src="../js/projects.js" type="text/javascript"></script>
        <script src="../js/parallax.js" type="text/javascript"></script>
        <script src="../js/planets.js" type="text/javascript"></script>
        <script src="../js/scroll-project.js" type="text/javascript"></script>
    </head>
    <body>
      <div class="fade-layer"></div>
    <?php include(dirname(__DIR__)."/html/project_commandes.php") ?>
