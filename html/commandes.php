<div class="commandes">

    <div class="mobile">
        <i class="fas fa-times fa-2x"></i>
        <img class="mobile_tab" src="img/interface/onglet-gauche.png">
    </div>
    <div class="left">
        <div class="btn">
            <div class="commande">
                <a style="cursor: pointer"><img src="img/interface/ecran-gauche.png"/></a>
            </div>
            <div class="text">
                <a style="cursor: pointer"><img src="img/interface/informations.png"/></a>
            </div>
        </div>
        <div class="tab" style="display:none">
            <img src="img/interface/onglet-gauche.png">
            <div class="text">
                <span class="category">Expérience Professionnelle</span>

                <div class="experience">
                    <div class="title">Agence ThisWay</div>
                    <p class="content">
                        5 mois : Contrat de professionnalisation <br/>
                        6 mois : Stage <br/>
                        en création de contenus web & réseaux sociaux
                    </p>
                    <p class="content"></p>
                </div>
                <div class="experience">
                    <div class="title">Concours</div>
                    Participation au concours Saxoprint : Resto du cœur
                </div>
                <div class="experience">
                    <div class="title">Projexion</div>
                    4 mois : Stage de communication visuelle
                </div>
                <div class="experience">
                    <div class="title">Flexineo</div>
                    4 mois : Stage de communication visuelle
                </div>
                <div class="experience">
                    <div class="title">Aleotech.io</div>
                    Travail de créations web en équipe avec des développeurs
                </div>
                <div class="experience">
                    <div class="title">Fête de l'anim'</div>
                    Participation au marathon de l'anim' deux années consécutives
                </div>

                <div class="experience">
                    <div class="title">Kiloutou</div>
                    Création de logo pour le service client
                </div>

                <span class="category">Formation</span>

                <div class="experience">
                    <div class="title">2017-2018 : Double cursus HEC</div>
                    e-commerce (Carrefour), Data (Unibail), Digital consumer (L’Oréal)
                </div>

                <div class="experience">
                    <div class="title">2014 - 2019 : e-artsup</div>
                    5ème année à e-artsup Paris en design graphique et communication visuelle
                </div>

                <div class="experience">
                    <div class="title">2013 - 2014 : Lycée Faidherbe</div>
                    Obtention du baccalauréat scientifique
                </div>
            </div>

        </div>
    </div>




    <div class="bottom">
        <!--        <img src="img/Interface/Interface.png"/>-->
        <div class="bottom-area"></div>
        <img src="img/interface/interface-bas.png"/>
    </div>
    <div class="right">
      <div class="btn">
          <div class="commande">
              <a style="cursor: pointer"><img src="img/interface/ecran-droite.png"/></a>
          </div>
          <div class="text">
              <a style="cursor: pointer"><img src="img/interface/contact.png"/></a>
          </div>
      </div>
      <div class="tab" style="display:none">
        <img src="img/interface/onglet-droit.png">
        <div class="text">
          <div class="form">
            <span class="form-title">Contact :</span>
            <form method="POST" action="#">
              <input disabled class="form-control inp-contact" type="text" name="inp_name" placeholder="Nom">
              <input disabled class="form-control inp-contact" type="email" name="inp_email" placeholder="Adresse email">
              <textarea disabled class="form-control textarea-contact" name="inp_message" rows="5" placeholder="Message"></textarea>
              <div class="btn-contact-container"> <button disabled type="submit" class="btn btn-contact">Envoyer</button> </div>
            </form>
          </div>
          <div class="description">
            <div class="coordinates">
              06 06 06 06 06
              <br/>
              contact@arthurjeunechamp.com
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="minimap-btn">
        <img src="img/interface/bouton-carte.png"/>
    </div>
    <div class="planet_desc">
        <img src="img/interface/ecran-haut.png"/>
        <div class="description-container">
            <div class="description-sub-container">
                <h2 class="title"></h2>
                <p class="tags"></p>
                <p class="description"></p>
            </div>
        </div>
    </div>
</div>
