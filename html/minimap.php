<img src="img/map/ecran-soleil.png" style="width: 100vw;height: 100vh;object-fit: cover;">

<div class="map-planets">
    <div class="map-planets-container">
        <div class="row-left">
            <div class="planet-container">
                <div class="planet-informations">
                    <div class="planet-title">
                        <span>Liftier</span>
                    </div><div class="planet-description">
                    <span>#Print #Illustration #UX #UI #Matérialisation</span>
                </div>
                </div>
                <div class="map-planet">
                    <svg width="100%" height="100%" viewBox="0 0 509 534" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:1.5;">
                        <a href="projects/liftier.php"><image xlink:href="img/map/planete-miniature-liftier.png" height="100%" width="100%"/></a>
                        <g transform="matrix(1,0,0,1,-289.167,-463.054)">
                            <g transform="matrix(1,0,0,0.795238,0,0)">
                                <g id="contour-haut" transform="matrix(1,0,0,1,-1226.39,0)">
                                    <g opacity="0.25">
                                        <g transform="matrix(0.885997,0,0,1.11413,1248.62,122.443)">
                                            <circle cx="588.345" cy="727.298" r="284.983" style="fill:none;stroke:rgb(0,238,255);stroke-width:4.14px;"/>
                                        </g>
                                        <g transform="matrix(1,0,0,1.25749,0,0)">
                                            <path d="M1769.89,489.26L1769.89,464.495" style="fill:none;stroke:rgb(0,238,255);stroke-width:2.88px;"/>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
            </div>

            <div class="planet-container">
                <div class="planet-informations">
                    <div class="planet-title">
                        <span>Gonschä Chocolat</span>
                    </div><div class="planet-description">
                    <span>#Print #UX #UI #Displays #RéseauxSociaux</span>
                </div>
                </div>
                <div class="map-planet">
                    <svg width="100%" height="100%" viewBox="0 0 509 534" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:1.5;">
                        <a href="../projects/gonscha.php"><image xlink:href="img/map/planete-miniature-gonscha.png" height="100%" width="100%"/></a>
                        <g transform="matrix(1,0,0,1,-289.167,-463.054)">
                            <g transform="matrix(1,0,0,0.795238,0,0)">
                                <g id="contour-haut" transform="matrix(1,0,0,1,-1226.39,0)">
                                    <g opacity="0.25">
                                        <g transform="matrix(0.885997,0,0,1.11413,1248.62,122.443)">
                                            <circle cx="588.345" cy="727.298" r="284.983" style="fill:none;stroke:rgb(0,238,255);stroke-width:4.14px;"/>
                                        </g>
                                        <g transform="matrix(1,0,0,1.25749,0,0)">
                                            <path d="M1769.89,489.26L1769.89,464.495" style="fill:none;stroke:rgb(0,238,255);stroke-width:2.88px;"/>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="planet-container">
                <div class="planet-informations">
                    <div class="planet-title">
                        <span>Bajoka</span>
                    </div>
                    <div class="planet-description">
                        <span>#UX #UI</span>
                    </div>
                </div>
                <div class="map-planet">
                    <svg width="100%" height="100%" viewBox="0 0 509 534" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:1.5;">
                        <a href="projects/afrique.php"><image xlink:href="img/map/planete-miniature-afrique.png" height="100%" width="100%"/></a>
                        <g transform="matrix(1,0,0,1,-289.167,-463.054)">
                            <g transform="matrix(1,0,0,0.795238,0,0)">
                                <g id="contour-haut" transform="matrix(1,0,0,1,-1226.39,0)">
                                    <g opacity="0.25">
                                        <g transform="matrix(0.885997,0,0,1.11413,1248.62,122.443)">
                                            <circle cx="588.345" cy="727.298" r="284.983" style="fill:none;stroke:rgb(0,238,255);stroke-width:4.14px;"/>
                                        </g>
                                        <g transform="matrix(1,0,0,1.25749,0,0)">
                                            <path d="M1769.89,489.26L1769.89,464.495" style="fill:none;stroke:rgb(0,238,255);stroke-width:2.88px;"/>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="planet-container">
                <div class="planet-informations">
                    <div class="planet-title">
                        <span>Projexion</span>
                    </div>
                    <div class="planet-description">
                        <span>#Print #Illustration #UX #UI #Motion #Matérialisation</span>
                    </div>
                </div>
                <div class="map-planet">
                    <svg width="100%" height="100%" viewBox="0 0 509 534" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:1.5;">
                        <a href="projects/projexion.php"><image xlink:href="img/map/planete-miniature-projexion.png" height="100%" width="100%"/></a>
                        <g transform="matrix(1,0,0,1,-289.167,-463.054)">
                            <g transform="matrix(1,0,0,0.795238,0,0)">
                                <g id="contour-haut" transform="matrix(1,0,0,1,-1226.39,0)">
                                    <g opacity="0.25">
                                        <g transform="matrix(0.885997,0,0,1.11413,1248.62,122.443)">
                                            <circle cx="588.345" cy="727.298" r="284.983" style="fill:none;stroke:rgb(0,238,255);stroke-width:4.14px;"/>
                                        </g>
                                        <g transform="matrix(1,0,0,1.25749,0,0)">
                                            <path d="M1769.89,489.26L1769.89,464.495" style="fill:none;stroke:rgb(0,238,255);stroke-width:2.88px;"/>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
            </div>
        </div>
        <div class="row-right">
            <div class="planet-container">
                <div class="map-planet">
                    <svg width="100%" height="100%" viewBox="0 0 509 534" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:1.5;">
                        <a href="projects/act.php"><image xlink:href="img/map/planete-miniature-act.png" height="100%" width="100%"/></a>
                        <g transform="matrix(1,0,0,1,-600.868,-1041.43)">
                            <g transform="matrix(1,0,0,0.795238,0,0)">
                                <g id="contour-bas" transform="matrix(-1,1.53998e-16,-9.73886e-17,-1,2625.08,2562.14)">
                                    <g opacity="0.25">
                                        <g transform="matrix(0.885997,0,0,1.11413,1248.62,122.443)">
                                            <circle cx="588.345" cy="727.298" r="284.983" style="fill:none;stroke:rgb(0,238,255);stroke-width:4.14px;"/>
                                        </g>
                                        <g transform="matrix(1,0,0,1.25749,0,0)">
                                            <path d="M1769.89,489.26L1769.89,464.495" style="fill:none;stroke:rgb(0,238,255);stroke-width:2.88px;"/>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
                <div class="planet-informations">
                    <div class="planet-title">
                        <span>Act responsible</span>
                    </div>
                    <div class="planet-description">
                        <span>#Print #Brochure #Panneaux #Matérialisation</span>
                    </div>
                </div>
            </div>
            <div class="planet-container">
                <div class="map-planet">
                    <svg width="100%" height="100%" viewBox="0 0 509 534" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:1.5;">
                        <a href="projects/pascroises.php"><image xlink:href="img/map/planete-miniature-pascroises.png" height="100%" width="100%"/></a>
                        <g transform="matrix(1,0,0,1,-600.868,-1041.43)">
                            <g transform="matrix(1,0,0,0.795238,0,0)">
                                <g id="contour-bas" transform="matrix(-1,1.53998e-16,-9.73886e-17,-1,2625.08,2562.14)">
                                    <g opacity="0.25">
                                        <g transform="matrix(0.885997,0,0,1.11413,1248.62,122.443)">
                                            <circle cx="588.345" cy="727.298" r="284.983" style="fill:none;stroke:rgb(0,238,255);stroke-width:4.14px;"/>
                                        </g>
                                        <g transform="matrix(1,0,0,1.25749,0,0)">
                                            <path d="M1769.89,489.26L1769.89,464.495" style="fill:none;stroke:rgb(0,238,255);stroke-width:2.88px;"/>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
                <div class="planet-informations">
                    <div class="planet-title">
                        <span>Pas croisés</span>
                    </div>
                    <div class="planet-description">
                        <span>#Print #Affiches #Brochure #UX #UI</span>
                    </div>
                </div>
            </div>
            <div class="planet-container">
                <div class="map-planet">
                    <svg width="100%" height="100%" viewBox="0 0 509 534" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:1.5;">
                        <a href="projects/restos.php"><image xlink:href="img/map/planete-miniature-restos.png" height="100%" width="100%"/></a>
                        <g transform="matrix(1,0,0,1,-600.868,-1041.43)">
                            <g transform="matrix(1,0,0,0.795238,0,0)">
                                <g id="contour-bas" transform="matrix(-1,1.53998e-16,-9.73886e-17,-1,2625.08,2562.14)">
                                    <g opacity="0.25">
                                        <g transform="matrix(0.885997,0,0,1.11413,1248.62,122.443)">
                                            <circle cx="588.345" cy="727.298" r="284.983" style="fill:none;stroke:rgb(0,238,255);stroke-width:4.14px;"/>
                                        </g>
                                        <g transform="matrix(1,0,0,1.25749,0,0)">
                                            <path d="M1769.89,489.26L1769.89,464.495" style="fill:none;stroke:rgb(0,238,255);stroke-width:2.88px;"/>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
                <div class="planet-informations">
                    <div class="planet-title">
                        <span>Restos du coeur</span>
                    </div>
                    <div class="planet-description">
                        <span>#Print #Affiches</span>
                    </div>
                </div>
            </div>
            <div class="planet-container">
                <div class="map-planet">
                    <svg width="100%" height="100%" viewBox="0 0 509 534" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:1.5;">
                        <a href="projects/mateo.php"><image xlink:href="img/map/planete-miniature-mateo.png" height="100%" width="100%"/></a>
                        <g transform="matrix(1,0,0,1,-600.868,-1041.43)">
                            <g transform="matrix(1,0,0,0.795238,0,0)">
                                <g id="contour-bas" transform="matrix(-1,1.53998e-16,-9.73886e-17,-1,2625.08,2562.14)">
                                    <g opacity="0.25">
                                        <g transform="matrix(0.885997,0,0,1.11413,1248.62,122.443)">
                                            <circle cx="588.345" cy="727.298" r="284.983" style="fill:none;stroke:rgb(0,238,255);stroke-width:4.14px;"/>
                                        </g>
                                        <g transform="matrix(1,0,0,1.25749,0,0)">
                                            <path d="M1769.89,489.26L1769.89,464.495" style="fill:none;stroke:rgb(0,238,255);stroke-width:2.88px;"/>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
                <div class="planet-informations">
                    <div class="planet-title">
                        <span>Matéo</span>
                    </div>
                    <div class="planet-description">
                        <span>#UX #UI</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="minimap-exit-btn">
    <img src="img/map/retour-accueil.png"/>
</div>


