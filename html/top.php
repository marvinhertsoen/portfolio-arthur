<!DOCTYPE html>

<html>
    <head>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/commandes.css" rel="stylesheet">

        <link href="css/rwd_commandes.css" rel="stylesheet">
        <link href="css/rwd_planets.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">


        <script src="js/vendor/jquery.min.js" type="text/javascript"></script>
        <script src="js/vendor/jquery.mobile.events.min.js" type="text/javascript"></script>
        <script src="js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/vendor/jquery.lazy.min.js" type="text/javascript"></script>

        <script src="js/vendor/appear.js" type="text/javascript"></script>
        <script src="js/helper.js" type="text/javascript"></script>

    </head>
    <body>
