$(document).ready(function(){
    /*
    *   auto scroll for project presentation
    */
    $(".commandes .bottom img").click(function(e){
        e.preventDefault();
        let $win = $(window);

        //if viewport isn't at page bottom, scroll normally
        if(!(($win.scrollTop() + $win.height()) === $(document).height())){
            $("html, body").animate({ scrollTop: $win.scrollTop() + $win.height() }, 1000);
        }
        // else scroll back to top of the page
        else{
            $("html, body").animate({ scrollTop: 0 }, 1000);
        }
    });
});
