$(document).ready(function(){

    /*
    *   Check if element is in viewport
     */
    $.fn.isInViewport = function() {
        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight();
        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();
        return elementBottom > viewportTop && elementTop < viewportBottom;
    };

    let active_sections = [];


    /*
    *   Set sections as active or not
     */
    function updateSectionsStatus(){
      active_sections = [];

      document.querySelectorAll('section').forEach(function(section) {
         if ( $(section).isInViewport())   {
           section.classList.add("active")
         }
         else{
           section.classList.remove("active")

         }
      });
    }

    /*
    *   auto scroll next section when on click bottom button / go back to top if no one else
     */
    $(".commandes .bottom img, .commandes .bottom .bottom-area").click(function(e){

        let next_section = $("section.active:first").next("section");
        console.log(next_section);
        if(next_section.length){
            console.log("#"+$(next_section)[0].id);
            $("html, body").stop().animate( { scrollTop: $("#"+$(next_section)[0].id).offset().top }, 1000);
        }
        else{
            $("html, body").stop().animate( { scrollTop: $("#1").offset().top }, 1000);
        }
    });


    /*
    *   Main scroll listener
     */

    $(window).on('resize scroll', function() {
      updateSectionsStatus();
      updateSunPinPos();
      updateShadowsPos();
    });



    /*
    *   Manage sun pin speed and position
     */
    let sun_pin = document.getElementsByClassName('sun_pin')[0];

    firstTop = $(".sun_pin").offset().top;
    var scrollTop;
    var imgPos;
    function updateSunPinPos(){
      scrollTop = window.scrollY;
      imgPos = scrollTop/1.178 +"px";
      sun_pin.style.transform = 'translateY(' + imgPos + ')';
    }


    /*
    *   Enable shadow when section in enabled
     */
    let e1;                                                         // first element
    let e2;                                                         // second element
    let angleDeg;                                                   // angle of both elements
    let limit = 0;                                                  // limit computational load

    function updateShadowsPos(){
      if (limit < 1 ){ limit++; }
      else{
          e2 = getPositionAtCenter(sun_pin);
          document.querySelectorAll('section.active .shadow.rotating img').forEach(function(shadow) {
              e1 = getPositionAtCenter(shadow);
              angleDeg = Math.atan2(e2.y - e1.y, e2.x - e1.x) * 120 / Math.PI;
              shadow.style.webkitTransform = 'rotate(' + angleDeg + 'deg)'
          });
          limit=0;
      }
    }

    function getPositionAtCenter(element) {
        const {top, left, width, height} = element.getBoundingClientRect();
        return {
            x: left + width / 2,
            y: top + height / 2
        };
    }

    /**
     * Parallax for space elements
     */

    if($(window).width() > 768){
        $(".rellax").each(function(e){
            wrapper = "." + $(this).attr("wrapper");
            elem_speed = $(this).attr("speed") ? $(this).attr("speed") : -2 ;
            new Rellax(this, {
                wrapper: wrapper,
                relativeToWrapper: true,
                speed: elem_speed,          // In HTML element attributes
                vertical: true,
                horizontal: false
            });
        });
    }

});
