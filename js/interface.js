$(document).ready(function(){

    /**
     * Quand on clique sur un bouton Contact ou information
     */
    $(document).click(function(event){

        //disable scroll
        handleScroll("hidden");

        let btn = $(event.target).closest(".commandes .btn");
        let btns = $(".commandes .btn");
        if(btn.length){
            let tab = $(btn).next(".tab");
            tab_transition_params = interactTab(tab, "open");


            $(tab).addClass("active");
            $(btns).addClass("btn-hidden");
            $(btns).find("img").animate(             { "bottom": "-=400px" }, "slow" );

            if ($(window).width() < 768) {
                $(".mobile_tab").show();
                $(tab).find(".text").addClass("text-mobile");
                $(tab).find(".text").show();
                $(".mobile i").show();

                $(tab).show().find("img").hide();

            }

            else{
                $(tab).show().find("img").show().animate(    tab_transition_params, "slow" );
                $(".mobile_tab").hide();
                $(tab).find(".text").removeClass("text-mobile");
                $(tab).find(".text").animate(              tab_transition_params, "slow" );

            }

        }
    });

    /**
     * Quand on clique hors des zones de tab, fermeture des tab ouverts
     */
    $(document).click(function(event){

        if(!$(event.target).closest(".left, .right").length){
            //enable scroll
            handleScroll("visible");

            $(".commandes .left .tab.active, .commandes .right .tab.active").each(function(e){
                let tab = $(this);
                let btn = $(tab).prev(".btn");
                let btns = $(".commandes .btn");

                tab.removeClass("active");

                if ($(window).width() < 768) {
                    $(".mobile_tab").hide();
                    $(tab).find(".text").css("display", "unset");
                    $(".mobile i").hide();
                    $(tab).find(".text").removeClass("text-mobile");

                }

                else{
                    tab_transition_params = interactTab(tab, "close");
                    tab.find(".text").animate(       tab_transition_params, "slow" );
                    tab.find("img").animate(    tab_transition_params, "slow", function () {
                        tab.hide();
                    });

                    $(".mobile_tab").hide();

                }

                btns.find("img").animate( { "bottom": "+=400px" }, "slow", function(){
                    $(this).css("bottom",""); // end of animation : remove bottom style attr of btn element (for rwd)
                });
            });
       }
    });

    function interactTab(tab, action){
        if     ($(tab).parent(".left").length && action== "open")   {console.log("1"); return {"left" : "+=40%"};}    //50% total
        else if($(tab).parent(".left").length && action== "close")  {console.log("2"); return {"left" : "-=40%"};}
        else if($(tab).parent(".right").length && action== "open")  {console.log("3"); return {"right" : "+=40%"};}
        else if($(tab).parent(".right").length && action== "close") {console.log("4"); return {"right" : "-=40%"};}

        else {console.log("error tab trigger");}
    }



    /**
     * Grossissement des boutons d'interface on hover
     */
    $(".commandes .btn img").hover(function(){
        $(this).closest(".btn ").find("img").css("transform","scale(1.05)");
    },function(){
        $(this).closest(".btn ").find("img").css("transform","scale(1.0)");
    });


    /**
     * Si on provient d'une planète, on lance l'animation de fade
     */
    if(getUrlParameter("fade-layer")){
        $( ".fade-layer" ).css("opacity","1");
        $( ".fade-layer" ).animate({
            opacity: 0,
        }, 2000, function() {
        });
    }

});
