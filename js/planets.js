
$(document).ready(function(){


    /**
     * Lazy load of images at startup
     */
    $(function() {
        $('.lazy').Lazy({
            removeAttribute: false // Do not delete img attribute data-src (used constuct hover img url)
        });
    });

    if( $(window).width() > 768){
        /**
         * Change planet image on hover
         */
        $("section .planet.hashover .main.clickable img").hover(function(){
            planet_src         = $(this).attr('data-src');
            planet_hover_src   = planet_src.split('.').slice(0, -1).join('.') + "-hover.png";

            console.log(planet_hover_src);
            $(this).attr('src', planet_hover_src);

        }, function(){
            $(this).attr('src', planet_src);

        });


        /**
         * display planet description on hover
         */
        $(".planet.hashover .main img").hover(function(){

            self = this;
            timeoutId = window.setTimeout(function() {
                $planet = $(self).closest(".planet");
                $title = $planet.attr("desc_title");
                $tags = $planet.attr("desc_tags");
                $desc = $planet.attr("desc_description");
                $(".planet_desc img, .planet_desc .description-container").finish().animate({
                    top: "+=270px",
                }, 200, function() { });

                $(".planet_desc .title").html($title);
                $(".planet_desc .tags").html($tags);
                $(".planet_desc .description").html($desc);
            }, 300);


        }, function() {

            $(".planet_desc img, .planet_desc .description-container").finish().animate({
                top: "-=270px",
            }, 400, function() {
            });
        });


        /**
         * Load project page on planet click + animations
         */
        $("section .planet .main.clickable img").click(function(){
            let planet = $(this).closest(".planet");
            $( ".fade-layer" ).animate({
                opacity: 1,
            }, 1500, function() {
                //redirect to project planet when animation is finished
                redirectToPlanet(planet);
            });

        });

    }



    if( $(window).width() < 768){
        let redirect = false;

        /* disable context menu on mobile devices */
        window.oncontextmenu = function(event) {
            event.preventDefault();
            event.stopPropagation();
            return false;
        };

        /**
         * Change planet image on hover
         */
        planet_src = "";
        $("section .planet .main.clickable img").on("tapstart",function(){
            planet_src         = $(this).attr('data-src');
            planet_hover_src   = planet_src.split('.').slice(0, -1).join('.') + "-hover.png";
            $(this).attr('src', planet_hover_src);
        });
        $("section .planet .main.clickable img").on("tapend swipeend",function(){
            $(this).attr('src', planet_src);
        });


        /**
         * Change planet scale on hover
         */
        $(".planet.hashover .main img").on("tapstart", function(){
            $(this).css("transform","scale(1.1)");
        });
        $(".planet.hashover .main img").on("tapend, swipeend", function(){
            $(this).css("transform","scale(1.0)");
        });


        /**
         * display planet description on hover
         */
        $("section .planet .main.clickable img").on("tapstart", function(){
            console.log("start");
            self = this;
            timeoutId = window.setTimeout(function() {
                $planet = $(self).closest(".planet");
                $title = $planet.attr("desc_title");
                $tags = $planet.attr("desc_tags");
                $desc = $planet.attr("desc_description");
                $(".planet_desc img, .planet_desc .description-container").finish().animate({
                    top: "+=270px",
                }, 200, function() { });

                $(".planet_desc .title").html($title);
                $(".planet_desc .tags").html($tags);
                $(".planet_desc .description").html($desc);
            }, 300);
        });

        $("section .planet .main.clickable img").on("tapend", function(e){
            $(".planet_desc img, .planet_desc .description-container").finish().animate({
                top: "-=270px",
            }, 400, function() {
            });
        });


        /**
         * Load project page on planet click + animations
         */
        $("section .planet .main.clickable img").on("tapmove",function(){
            redirect = false;
        });

        $("section .planet .main.clickable img").on("tap",function(){
            redirect = true;
        });

        $("section .planet .main.clickable img").on("tapend",function(){
            if(redirect){
                let planet = $(this).closest(".planet");
                $( ".fade-layer" ).animate({
                    opacity: 1,
                }, 1500, function() {
                    //redirect to project planet when animation is finished
                    redirectToPlanet(planet);
                });
            }
        });
    }


    /**
     * Redirect user to selected planet
     */
    function redirectToPlanet(planet){
        let project_url = $(planet).attr("project_url");
        if(project_url){location.href = "projects/"+project_url;}
        else{console.log("no url associated")}
    }

});
