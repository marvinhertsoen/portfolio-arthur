

/*********************************** EVENTS ***********************************/

$(document).ready(function(){

  // Landing animation at display start
  landing();

  // LiftOff animation when click on 'decollage'
  $(".launch img").click(function(){
    $(this).css("transform", ""); // reset btn scale
    liftOff();
  });
});


/*********************************** FUNCTIONS ***********************************/


/**
 * Landing animation
 */
function landing(){

  // ANIMATIONS FOR :  Interface + Fade Layer + presentation img
  $( ".fade-layer" ).animate({
    opacity: 0,
  }, 1500, function() {
  });

  $( ".launch img" ).animate({
    right: "+=300px",
  }, 1500, function() {
  });

  $( ".presentation" ).animate({
    bottom: "+=1500px",
  }, 1750, function() {
  });

  // ANIMATIONS FOR :  Background Layers
  $( ".background-1 img" ).animate({
    bottom: "+=1000px",
  }, 1750, function() {
  });

  $( ".background-2 img" ).animate({
    bottom: "+=600px",
  }, 1750, function() {
  });

  $( ".background-3 img" ).animate({
    bottom: "+=300px",
  }, 1750, function() {
  });

  $( ".background-main img" ).animate({
    bottom: "+=100px",
  }, 1750, function() {
  });
}

function liftOff(){

  // ANIMATIONS FOR :  Fade Layer, Interface & presentation image opacity
  $( ".fade-layer" ).animate({
    opacity: 1,
  }, 2000, function() {
    //redirect to home-page when animation is finished
    redirectHome();
  });

  $( ".launch img" ).animate({
    right: "-=300px",
  }, 1000, function() {
  });

  $( ".presentation" ).animate({
    bottom: "-=1500px",
    opacity: 0,
  }, 2000, function() {
  });

  // ANIMATIONS FOR :  Background Layers

  $( ".background-1 img" ).animate({
    bottom: "-=1000px",
  }, 2000, function() {
  });

  $( ".background-2 img" ).animate({
    bottom: "-=600px",
  }, 2000, function() {
  });

  $( ".background-3 img" ).animate({
    bottom: "-=300px",
  }, 2000, function() {
  });

  $( ".background-main img" ).animate({
    bottom: "-=100px",
  }, 2000, function() {
  });
}

function redirectHome(){
  let section_id = $(".project").attr("section_id");
  console.log(section_id);
  if(section_id){location.href = "../"+"?fade-layer=true"+"#"          +section_id; console.log("ok");}
  else          {location.href = "../";                                          console.log("nok");}
}
