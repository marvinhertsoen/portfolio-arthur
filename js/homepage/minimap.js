$(document).ready(function(){

    $(".minimap-btn img").click(function(){
        // disable all minimap's neighbors
        $(".minimap ~ div").css("display", "none");
        $("body").css("background", "none");
        $(".minimap").css("display", "block");
    });

    $(".minimap-exit-btn img").click(function(){
        // enable all minimap's neighbors
        $(".minimap ~ div").css("display", "block");
        $("body").css("background", "");
        $(".minimap").css("display", "none");
    });

});