<?php include("html/top.php");?>

<div class="cover"></div>
<div class="minimap">
    <?php include("html/minimap.php");?>
</div>
<div class="background-sun"><!--<img src="img/Elements%20espace/Soleil_2mo.png"/>--></div>
<div class="sun_pin" ><!--AAA--></div>
<div class="fade-layer"></div>


<?php include("html/commandes.php") ?>
<?php //echo "Bonjour ! :) ";?>
<div class="sections_container">
    <section class="home scroll-cursor active" id="1">
        <div class="text-center align-middle">
            <h1>Préparez-vous</h1>
            <h2>à voyager à travers</h2>
            <h2>de nombreux univers graphique</h2>
        </div>
    </section>


    <section class="intro" id="2">
        <div class="planet planet-intro ">
            <div class="main">
                <img class="lazy" data-src="img/planetes+ombres/V2/arthur-planete-ss.png"/>
            </div>
            <div class="shadow">
                <img class="lazy" data-src="img/planetes+ombres/V2/arthur-ombre.png"/>
            </div>
        </div>
    </section>

    <section class="sec1" id="3">
        <div class="planet planet-1-1 hashover"
             project_url="liftier.php"
             desc_title="Liftier"
             desc_tags="#Print #Illustration #UX #UI #Matérialisation"
             desc_description="Liftier est une application qui, à travers l’intelligence artificielle, améliore les échanges humains en interne,
             optimise la gestion du temps et comprend les désirs de chacun pour faciliter la bonne communication">

            <div class="main clickable">
                <img class="lazy" data-src="img/planetes+ombres/V2/liftier-planete-ss.png"/>
            </div>
            <div class="shadow">
                <img class="lazy" data-src="img/planetes+ombres/V2/liftier-ombre.png"/>
            </div>
        </div>
        <div class="space-element asteroid">
            <img class="rellax" wrapper="sec1" speed="-5" src="img/elements-espace/asteroid-3.png" alt="image">
        </div>

    </section>

    <section class="sec2" id="4">
        <div class="planet planet-2_1-1 hashover"
             project_url="act.php"
             desc_title="Act Responsible"
             desc_tags="#Print #Brochure #Panneau #Matérialisation"
             desc_description="Act Responsible a pour mission d'inspirer, promouvoir et fédérer les acteurs de la communication publicitaire autour de la responsabilité sociale, du développement durable et de partager les bonnes pratiques.">
            <div class="main clickable">
                <img class="lazy" data-src="img/planetes+ombres/V2/act-planete-ss.png"/>
            </div>
            <div class="shadow">
                <img class="lazy" data-src="img/planetes+ombres/V2/act-ombre.png"/>
            </div>
        </div>
        <div class="planet planet-2_1-2 hashover"
             project_url="gonscha.php"
             desc_title="Gonschä Chocolat"
             desc_tags="#Print #UX #UI #Bannières #RéseauxSociaux"
             desc_description="GONSCHÄ Chocolat est une marque de chocolat qui se veut luxueuse, artisanale et biologique. C’est à travers le packaging que l’histoire qui tourne autour de la fabrication est racontée.">
            <div class="main clickable">
                <img class="lazy" data-src="img/planetes+ombres/V2/gonscha-planete-ss.png"/>
            </div>
            <div class="shadow rotating">
                <img class="lazy" data-src="img/planetes+ombres/V2/gonscha-ombre.png"/>
            </div>
        </div>
    </section>


    <section class="sec3" id="5">
        <div class="planet planet-2_2-1 hashover"
             project_url="pascroises.php"
             desc_title="Pas Croisés"
             desc_tags="#Print #Affiches #Brochure #UX #UI"
             desc_description="Pas Croisés est un festival mondial de danse se déroulant à Lille. Le rendu visuel s'inspire du corps et de la voix pour mélanger à la fois les mouvements et la puissance des mots.">
            <div class="main clickable">
                <img class="lazy" data-src="img/planetes+ombres/V2/pascroises-planete-ss.png"/>
            </div>
            <div class="shadow">
                <img class="lazy" data-src="img/planetes+ombres/V2/pascroises-ombre.png"/>
            </div>
        </div>
        <div class="planet planet-2_2-2 hashover"
             project_url="afrique.php"
             desc_title="CAL"
             desc_tags="#UX #UI"
             desc_description="CAL est une aventure interactive vous permettant de voyager à travers les différentes cultures d’Afrique. L’exploration portant principalement sur la création de bijoux.">
            <div class="main clickable">
                <img class="lazy" data-src="img/planetes+ombres/V2/afrique-planete-ss.png"/>
            </div>
            <div class="shadow rotating">
                <img class="lazy" data-src="img/planetes+ombres/V2/afrique-ombre.png"/>
            </div>
        </div>
        <div class="space-element asteroid-2">
            <img class="rellax" wrapper= "sec3" speed="-2" src="img/elements-espace/asteroid-2.png" alt="image">
        </div>
        <div class="space-element asteroid-chain">
            <img class="rellax" wrapper= "sec3" speed="-4.5" src="img/elements-espace/asteroid-chaines.png" alt="image">
        </div>
    </section>


    <section class="sec4" id="6">
        <div class="planet planet-3-1 hashover"
             project_url="mateo.php"
             desc_title="Matéo"
             desc_tags="#UX #UI"
             desc_description=" Matéo est une application météo qui transmet les informations que l'utilisateur souhaite vraiment savoir en consultant la météo. Pour être plus précis, sa façon avec laquelle il va prévoir sa journée.">
            <div class="main clickable">
                <img class="lazy" data-src="img/planetes+ombres/V2/mateo-planete-ss.png"/>
            </div>
            <div class="shadow">
                <img class="lazy" data-src="img/planetes+ombres/V2/mateo-ombre.png"/>
            </div>
        </div>
        <div class="planet planet-3-2 hashover"
             project_url="restos.php"
             desc_title="Restos du Cœur"
             desc_tags="#Print #Affiches"
             desc_description="Les Restos du Cœur fournissent des repas, mais proposent également un large panel de services que le public connait peu. C’est à travers un affichage 4x3 que le spectateur y est sensibilisé.">
            <div class="main clickable">
                <img class="lazy" data-src="img/planetes+ombres/V2/restos-planete-ss.png"/>
            </div>
            <div class="shadow rotating">
                <img class="lazy" data-src="img/planetes+ombres/V2/restos-ombre.png"/>
            </div>
        </div>
        <div class="planet planet-3-3 hashover"
             project_url="projexion.php"
             desc_title="Projexion"
             desc_tags="#Print #Illustration #UX #UI #Motion #Matérialisation"
             desc_description="Projexion est une société spécialisée en organisation, gestion de projets et architecture d’entreprise où s’organisent talents, technologies et méthodes au service des entreprises qui l'entoure.">
            <div class="main clickable">
                <img class="lazy" data-src="img/planetes+ombres/V2/projexion-planete-ss.png"/>
            </div>
            <div class="shadow rotating">
                <img class="lazy" data-src="img/planetes+ombres/V2/projexion-ombre.png"/>
            </div>
        </div>
        <div class="space-element asteroid-3">
            <img class="rellax" wrapper= "sec4" speed="-2" src="img/elements-espace/asteroid-3.png" alt="image">
        </div>
    </section>


    <section class="last" id="7">
        <div class="planet blackhole" project_url="blackhole.php" desc="Ceci est un trou noir !">
            <div class="main clickable">
                <img class="lazy" data-src="img/planetes+ombres/V2/Trou-planete-ss.png"/>
            </div>
        </div>
        <div class="planet satellite" project_url="video.php" desc="Ceci est spoutnik !">
            <div class="main clickable">
                <img class="lazy" data-src="img/planetes+ombres/V2/videos-planete-ss.png"/>
            </div>
        </div>
    </section>
</div>

<?php include("html/bottom.php");?>
